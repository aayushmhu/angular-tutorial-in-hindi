import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './containers/login.component';
import {DashboardComponent} from './containers/dashboard.component';
import {AuthGuard} from './guards/auth-guard';
import {AnonGuard} from './guards/anon-guard';


const routes: Routes = [
  {
    path: '', canActivate: [AnonGuard],
    children: [{path: '', component: LoginComponent}]
  },
  {
    path: '', canActivate: [AuthGuard], children: [{
      path: 'dashboard', component: DashboardComponent
    }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

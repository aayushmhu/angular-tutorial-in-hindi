import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthUtils} from '../utils/auth-utils';
import {Injectable} from '@angular/core';


@Injectable()
export class AnonGuard implements CanActivate{
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const isLoggedIn = !!AuthUtils.getAuthDetails();
    if (isLoggedIn) {
      this.router.navigate(['dashboard']);
    } else {
      return true;
    }
  }
}

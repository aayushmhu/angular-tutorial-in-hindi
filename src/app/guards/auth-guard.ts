import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthUtils} from '../utils/auth-utils';
import {Injectable} from '@angular/core';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const isLoggedIn = !!AuthUtils.getAuthDetails();
    if (isLoggedIn) {
      return true;
    } else {
      this.router.navigate(['']);
    }
  }
}

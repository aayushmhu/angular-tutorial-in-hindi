import {Injectable} from '@angular/core';
import {HttpService} from './http-service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AuthUtils} from '../utils/auth-utils';
import {User} from '../models/user';

@Injectable()
export class ApiService {
  constructor(private httpService: HttpService) {
  }

  getUserList(page = 1): Observable<User[]> {
    return this.httpService.get('/users?page=' + page)
      .pipe(map(res => res.data));
  }

  loginAndSetAuthDetails(data): Observable<any> {
    return this.httpService.post('/login', data)
      .pipe(map((res) => {
        AuthUtils.setAuthDetails();
        return res;
      }));
  }
}
